/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        
        int listSizeA = getListSize(headA);
        int listSizeB = getListSize(headB);
        ListNode *stepA = headA;
        ListNode *stepB = headB;
        
        
        if(listSizeA * listSizeB == 0) return NULL;
        
        int pre = 0;
        
        if(listSizeA > listSizeB) {
            pre = listSizeA-listSizeB;
            
            while(pre--) {
                stepA = stepA->next;
            }
        } else {
            pre = listSizeB-listSizeA;
            
            while(pre--) {
                stepB = stepB->next;
            }
        }
        
        while(stepA != NULL && stepB != NULL) {
            if(stepA == stepB) return stepA;
            
            stepA = stepA->next;
            stepB = stepB->next;
        }
        
        
        return NULL;
    }
    
private:
    int getListSize(ListNode *list) {
        int i = 0;
        ListNode *inter = list;
        while(inter) {
            i++;
            inter = inter->next;
        }
        return i;
    }
};