class Solution:
    # @param matrix, a list of lists of integers
    # RETURN NOTHING, MODIFY matrix IN PLACE.
    def setZeroes(self, matrix):
        h = []
        w = []
        for idx ,row in enumerate(matrix):
            for idy, column in enumerate(row):
                if column == 0:
                    h.append(idx)
                    w.append(idy)
        
        for idx ,row in enumerate(matrix):
            for idy, column in enumerate(row):
                if idx in h or idy in w:
                    matrix[idx][idy]=0
