/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        set<ListNode *> s;
        
        ListNode *start = head;
        
        while(start != NULL) {
            
            if(s.count(start) > 0) return true;
            s.insert(start);
            start = start->next;
        }
        
        return false;
        
    }
};