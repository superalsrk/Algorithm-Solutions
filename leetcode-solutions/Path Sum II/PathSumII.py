# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param root, a tree node
    # @param sum, an integer
    # @return a list of lists of integers
    def pathSum(self, root, s):
        allList = self.allPath(root)
        res = []
        
        for x in allList:
            if sum(x) == s:
                res.append(x)
        
        return res
    
    def allPath(self, root):
        if root is None:
            return []
        if root.left is None and root.right is None:
            return [[root.val]]
        
        leftRes = []
        rightRes = []
        if root.left is not None:
            leftRes = self.allPath(root.left)
        if root.right is not None:
            rightRes = self.allPath(root.right)
        
        tmp = leftRes + rightRes
        [element.insert(0, root.val) for element in tmp]
        return tmp

