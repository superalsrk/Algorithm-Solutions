public class Solution {
    public List<List<Integer>> generate(int numRows) {
        List <List<Integer>> res = new LinkedList<List<Integer>>();
        
        for(int i = 1; i <= numRows; i++) {
            
            List <Integer> tmp = new LinkedList<Integer>();
            tmp.add(1);
            
            if(i == 1) {
                res.add(tmp);
                continue;
            }
            
            List<Integer> upList = res.get(i-2);
            
            for(int j = 1; j < i-1; j++) {
                tmp.add(upList.get(j-1) + upList.get(j));
            }
            
            tmp.add(1);
            res.add(tmp);
            
        }
        
        return res;
    }
}