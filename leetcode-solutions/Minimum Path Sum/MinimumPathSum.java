public class Solution {
    public int minPathSum(int[][] grid) {
        
        int h = grid.length;
        int w = grid[0].length;
        
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[0].length; j++) {
                
                if(i == 0 && j == 0) continue;
                
                int upStep = (i == 0) ?999999 : grid[i-1][j];
                int leftStep = (j == 0) ? 999999 : grid[i][j-1];
                
                grid[i][j] = ((upStep < leftStep) ? upStep : leftStep) + grid[i][j];
            }
        }
        
        return grid[h-1][w-1];
        
    }
}