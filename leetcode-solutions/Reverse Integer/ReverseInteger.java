public class Solution {
    public int reverse(int x) {
        int tag = (x > 0) ? 1 : (-1);
        StringBuffer sb = new StringBuffer(String.valueOf(Math.abs(x)));
        Integer reverseNum = 0;
        try {
            reverseNum = Integer.parseInt(sb.reverse().toString());
        } catch (Exception e) {
            reverseNum = 0;
        }
        return reverseNum * tag;
    }
}