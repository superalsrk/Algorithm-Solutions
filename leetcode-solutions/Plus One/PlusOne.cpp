class Solution {
public:
    vector<int> plusOne(vector<int> &digits) {
        
        int tag = 1;
        for(int i = digits.size()-1; i>= 0; i--) {
            
            int total = tag + digits[i];
            digits[i] = total%10;
            tag = total/10;
        }
        if(tag != 0) digits.insert(digits.begin(),tag);
        return digits;
    }
};