# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param root, a tree node
    # @return an integer
    def sumNumbers(self, root):
        return sum([int(element) for element in self.totalList(root)])
    
    def totalList(self, root):
        
        if root == None:
            return []
        
        if (root.left == None) and (root.right == None):
            return [root.val]
        
        leftRes = []
        rightRes = []
        if root.left is not None:
            rightRes = self.totalList(root.left)
        
        if root.right is not None:
            leftRes = self.totalList(root.right)
        
        tmp = leftRes + rightRes
        return ["{0}{1}".format(root.val, element) for element in tmp]


