class Solution {
public:
    int removeDuplicates(int A[], int n) {
        if(n == 0) return 0;
        
        int step = 0;
        
        for(int i = 0; i < n; i++) {
            
            if(i == 0 || i > 0 && A[i-1] != A[i]) {
                A[step++] = A[i];
            }
        }
        return step;
    }
};