/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
        
        if(n == 0) return head;
        
        ListNode *backup = new ListNode(-1);
        backup->next = head;
        
        ListNode *front = backup;
        ListNode *after = backup;
        
        for(int i = 0; i < n; i++) {
            front = front->next;
        }
        
        while(front->next != NULL) {
            front = front->next;
            after = after->next;
        }
        
        after->next = after->next->next;
        
        return backup->next;
    }
};