class Solution:
    # @param version1, a string
    # @param version2, a string
    # @return an integer
    def compareVersion(self, version1, version2):
        v1 = version1.split('.') + ['0'] * 10
        v2 = version2.split('.') + ['0'] * 10
        
        for x in zip(v1,v2):
            if int(x[0]) > int(x[1]):
                return 1
            if int(x[0]) < int(x[1]):
                return -1

    return 0
