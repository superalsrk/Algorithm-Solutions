public class Solution {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        
        int[][] res = new int[m][n];
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                
                if(obstacleGrid[i][j] == 1) continue;
                
                if(i == 0 && j == 0) {
                    res[i][j] = 1;
                    continue;
                }
                
                
                int up = (i == 0 || obstacleGrid[i-1][j] == 1 ) ? 0 : res[i-1][j];;
                int left = (j == 0 || obstacleGrid[i][j-1] == 1 ) ? 0 : res[i][j-1];
                
                
                
                res[i][j] = up + left;
            }
        }
        
        return res[m-1][n-1];
    }
}