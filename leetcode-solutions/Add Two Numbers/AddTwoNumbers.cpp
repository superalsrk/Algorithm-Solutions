/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        int tag = 0;
        
        ListNode * s1 = l1;
        ListNode * s2 = l2;
        
        while(s1->next != NULL || s2->next != NULL) {
            
            if (s1->next == NULL) {
                s1->next = s2->next;
            } else if(s2->next == NULL) {
                s2->next = s1->next;
            }
            
            int sum = (s1 != s2) ? (s1->val+s2->val+tag) : (s1->val + tag);
            s1->val = sum%10;
            tag = sum/10;
            s1=s1->next;
            s2=s2->next;
        }
        
        int sum = (s1 != s2) ? (s1->val+s2->val+tag) : (s1->val + tag);
        s1->val = sum%10;
        tag = sum/10;
        
        if(tag > 0) {
            ListNode * tmp = new ListNode(tag);
            s2->next = tmp;
            s1->next = tmp;
        }
        return l1;
    }
};