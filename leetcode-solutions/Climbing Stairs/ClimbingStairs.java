public class Solution {
    public int climbStairs(int n) {
        if (n == 1) return 1;
        else if (n == 2) return 2;
        else {
            int p1 = 1;
            int p2 = 2;
            int tmp;
            for (int i = 3; i<= n ; i++) {
                    tmp = p2;
                    p2 = p2 + p1;
                    p1 = tmp;
            }
            return p2;
        }
    }
}