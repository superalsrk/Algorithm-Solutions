/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxDepth(TreeNode *root) {
        
        if (root == NULL) return 0;
        if(root->left == NULL && root->right == NULL) return 1;
        
        int leftDepth = (root->left != NULL) ? maxDepth(root->left) : (-9999);
        int rightDepth = (root->right != NULL) ? maxDepth(root->right) : (-9999);
        return  1 + (leftDepth > rightDepth ? leftDepth : rightDepth);
    }
};