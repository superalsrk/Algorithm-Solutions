public class Solution {
    public int strStr(String haystack, String needle) {
        int index = -1;
        if(needle.length() == 0) return 0;
        
        for(int i = 0; i < haystack.length() - needle.length() +1; i++) {
            if(needle.equals(haystack.substring(i, i+ needle.length()))){
                index = i;
                break;
            }
        }
        return index;
    }
}