public class Solution {
    public String longestCommonPrefix(String[] strs) {
        int prefixIndex = 0;
        
        if (strs == null || strs.length == 0)
            return "";
        while (true) {
            
            String prefix = "";
            
            if (prefixIndex > strs[0].length()) {
                return strs[0].substring(0, prefixIndex - 1);
            } else {
                prefix = strs[0].substring(0, prefixIndex);
            }
            
            for (int i = 0; i < strs.length; i++) {
                
                if (prefixIndex > strs[i].length()) {
                    return strs[0].substring(0, prefixIndex - 1);
                }
                
                if (!prefix.equals(strs[i].substring(0, prefixIndex))) {
                    return strs[0].substring(0, prefixIndex - 1);
                }
                
            }
            
            prefixIndex++;
        }
    }
}