public class Solution {
    public void sortColors(int[] A) {
        int redSize = 0;
        int whiteSize = 0;
        int blueSize = 0;
        
        int nowPos = 0;
        int beacon = 0;
        
        while(nowPos < A.length) {
            if(A[beacon] != 0 && A[nowPos] == 0) swap(A , beacon, nowPos);
            if(A[beacon] == 0) beacon++;
            nowPos++;
        }
        
        nowPos = beacon;
        while(nowPos < A.length) {
            if(A[beacon] != 1 && A[nowPos] == 1) swap(A, beacon, nowPos);
            if(A[beacon] == 1) beacon++;
            nowPos++;
        }
        
    }
    
    public void swap(int[] A, int i, int j) {
        int tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    }
}