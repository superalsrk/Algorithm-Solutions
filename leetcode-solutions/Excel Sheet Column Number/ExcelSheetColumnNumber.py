class Solution:
    # @param s, a string
    # @return an integer
    def titleToNumber(self, s):
        ret = 0;
        for idx,chr in enumerate(list(s)):
            ret += math.pow(26,len(s) - 1-idx) * (ord(chr)-ord('A')+1)
        return int(ret)
        
        
