class Solution:
    # @param x, a float
    # @param n, a integer
    # @return a float
    def pow(self, x, n):
        if n == 0:
            return 1.0
        if n == 1:
            return x
        if n == -1:
            return 1.0/x
        
        mid = n/2;
        tag = x
        if n%2 == 0:
            tag = 1
        half = self.pow(x,mid)
        return half * half * tag
