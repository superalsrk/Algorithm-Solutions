/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
        ListNode *pos1 = l1;
        ListNode *pos2 = l2;
        ListNode *start = new ListNode(-1);
        ListNode *pre = start;
        
        while(pos1 != NULL && pos2 != NULL) {
            if((pos1->val) < (pos2->val)) {
                pre->next = pos1;
                pre = pos1;
                pos1 = pos1->next;
            } else {
                pre->next = pos2;
                pre = pos2;
                pos2 = pos2->next;
            }
        }
        
        if(pos1 == NULL) {
            pre->next = pos2;
        }
        
        if(pos2 == NULL) {
            pre->next = pos1;
        }
        
        return start->next;
        
    }
};