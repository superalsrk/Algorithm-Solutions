public class Solution {
    public List<Integer> getRow(int rowIndex) {
        List<Integer> res = new ArrayList<Integer>();
        
        
        res.add(1);
        if(rowIndex == 0) return res;
        res.add(1);
        if(rowIndex == 1) return res;
        
        for(int i = 2; i <= rowIndex; i++) {
            
            for(int j = 1, pre = res.get(0); j < i; j++) {
                
                int tmp = res.get(j);
                res.set(j, pre + res.get(j));
                pre = tmp;
            }
            res.add(1);
            
        }
        return res;
    }
}
