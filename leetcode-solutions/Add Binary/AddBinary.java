public class Solution {
    public String addBinary(String a, String b) {
        
        int maxSize = a.length() > b.length() ? a.length() : b.length();
        StringBuffer sb = new StringBuffer();
        int tag = 0;
        
        String reverseA = new StringBuffer(a).reverse().toString();
        String reverseB = new StringBuffer(b).reverse().toString();
        
        for(int i = 0; i < maxSize; i++) {
            
            
            
            if(i < reverseA.length()) {
                tag += Integer.parseInt(String.valueOf(reverseA.charAt(i)));
            }
            
            if(i < reverseB.length()) {
                tag += Integer.parseInt(String.valueOf(reverseB.charAt(i)));
            }
            
            
            sb.append(String.valueOf(tag%2));
            tag = tag / 2;
            
        }
        
        if(tag != 0) {
            sb.append(String.valueOf(tag));
        }
        
        return sb.reverse().toString();
    }
}