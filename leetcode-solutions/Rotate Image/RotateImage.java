public class Solution {
    public void rotate(int[][] matrix) {
        
        int n = matrix.length;
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n/2; j++) {
                swap(matrix, i, j, i, n-1-j);
            }
        }
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n-i; j++){
                swap(matrix,i, j, n-1-j, n-1-i );
            }
        }
        
    }
    
    public void swap(int[][] matrix, int x1, int y1, int x2, int y2) {
        int tmp = matrix[x1][y1];
        matrix[x1][y1] = matrix[x2][y2];
        matrix[x2][y2] = tmp;
    }
}