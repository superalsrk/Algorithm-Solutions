class Solution:
    # @param s, a string
    # @return a string
    def reverseWords(self, s):
        arr = s.strip().split()[::-1]
        return ' '.join(arr)