Same Tree
===

> Given two binary trees, write a function to check if they are equal or not.
> Two binary trees are considered equal if they are structurally identical and the nodes have the same value.

递归算法，比较 节点值相等，左子树相同，右子树相同
