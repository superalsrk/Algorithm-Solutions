/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        
        ListNode *start = new ListNode(-100);
        start->next = head;
        ListNode *pre = start;
        ListNode *after = start;
        
        while(pre != NULL) {
            
            if(pre->val != after->val) {
                after->next = pre;
                after = after->next;
            }
            pre = pre->next;
            after->next = pre;
        }
        
        return start->next;
        
    }
};