/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isSymmetric(TreeNode *root) {
        if(root == NULL) return true;
        return isSubMirrorSame(root->left, root->right);
    }
    
    bool isSubMirrorSame(TreeNode *sub1, TreeNode *sub2) {
        
        if(sub1 == NULL && sub2 == NULL) return true;
        
        if(sub1 == NULL && sub2 != NULL || sub2 == NULL && sub1 != NULL || sub1->val != sub2->val) return false;
        
        if(sub1->val == sub2->val && sub1->left == NULL && sub1->right== NULL && sub2->left == NULL && sub2->right==NULL) return true;
        
        return isSubMirrorSame(sub1->left, sub2->right) && isSubMirrorSame(sub1->right, sub2->left);
    }
};