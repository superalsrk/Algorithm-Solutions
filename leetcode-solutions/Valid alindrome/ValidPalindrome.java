public class Solution {
    public boolean isPalindrome(String s) {
        
        if(s == null || s.trim().equals("")) return true;
        
        StringBuffer sb = new StringBuffer();
        
        for(int i = 0; i < s.length(); i++) {
            char tmp = s.charAt(i);
            if(tmp >= '0' && tmp <= '9' || tmp >='a' && tmp <= 'z' || tmp>='A' && tmp<= 'Z') {
                sb.append(String.valueOf(tmp).toUpperCase());
            }
        }
        return sb.toString().equals(sb.reverse().toString());
    }
}