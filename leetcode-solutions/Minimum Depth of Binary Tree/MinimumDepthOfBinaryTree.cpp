/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int minDepth(TreeNode *root) {
        
        if(root == NULL) return 0;
        if(root->left == NULL && root->right == NULL) return 1;
        
        int leftMinDepth = root->left != NULL ? minDepth(root->left) : 99999;
        
        int rightMinDepth = root->right != NULL ? minDepth(root->right) : 9999;
        
        return 1+((leftMinDepth < rightMinDepth) ? leftMinDepth : rightMinDepth);
    }
};