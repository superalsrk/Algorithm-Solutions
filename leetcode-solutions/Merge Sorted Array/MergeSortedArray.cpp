class Solution {
public:
    void merge(int A[], int m, int B[], int n) {
        
        for(int i = m+n-1; i >=n; i--) {
            A[i] = A[i-n];
        }
        
        int start = 0;
        int p1 = n;
        int p2 = 0;
        
        while(true) {
            if (p1 >= m+n) break;
            if (p2 >= n) break;
            
            if(A[p1] < B[p2]) {
                A[start++] = A[p1];
                p1++;
            } else {
                A[start++] = B[p2];
                p2++;
            }
        }
        
        while(p1 < m+n) {
            A[start++]=A[p1++];
        }
        
        while(p2 < n) {
            A[start++]=B[p2++];
        }
        
    }
};