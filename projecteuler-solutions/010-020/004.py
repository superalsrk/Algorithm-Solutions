import string
roots = []
for i in range(999,99,-1):
    for j in range(i,99,-1):
        s1 = str(i*j)
        s2 = s1[::-1]
        if s1 == s2:
            roots.append(string.atoi(s1, 10))

print max(roots)

'''
for i in range(999,99,-1):
    for j in range(i,99,-1):
        if str(i*j) == str(i*j)[::-1]:
            roots.append(i*j)
'''
