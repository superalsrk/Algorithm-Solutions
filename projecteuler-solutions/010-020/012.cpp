#include <iostream>
#include <cmath>
using namespace std;

int countFactors(int n) {
	int count = 0;
	for (int i = 1; i < (int)sqrt(n*1.0)+1; i++) {
		if(n%i == 0) count += 2;
		if(i*i == n) count --;
	}
	return count;
}

int main()
{
	int st = 1;
	while(1) {
		cout << st*(st+1)/2 << endl;
		if(countFactors(st*(st+1)/2) >= 500) {
			cout << st*(st+1)/2 << endl;
			break;
		}
		st++;
	}
	return 0;
}

// the good solution use pyhon is like this
/*
import math


def divisors(a):
    return [x for x in range(1, int(math.sqrt(a) + 1)) if a % x == 0]

n = 1
cur_triangle = 0
while True:
    cur_triangle += n
    div_count = len(divisors(cur_triangle))
    if div_count >= 250:
        print n, cur_triangle, div_count
        break
    n += 1
*/
