#we know that the number of primer_num between 1 and 1000000 is 78498
import math

def isPrime(n):
    for i in range(2, int(round(math.sqrt(n))) + 1):
        if n % i == 0:
            return False
    return True;

start = 2
num = 0

while True:
    if isPrime(start) == True:
        num = num + 1
        print start
    if num == 10001:
        break
    start = start + 1
