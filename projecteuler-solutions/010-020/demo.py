import math


def divisors(a):
    return [x for x in range(1, int(math.sqrt(a) + 1)) if a % x == 0]

n = 1
cur_triangle = 0
while True:
    cur_triangle += n
    div_count = len(divisors(cur_triangle))
    if div_count >= 250:
        print n, cur_triangle, div_count
        break
    n += 1