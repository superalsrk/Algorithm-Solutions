import math

def isPrime(num):
    for i in range(2,int(round(math.sqrt(num)))):
        if num % i == 0:
            return False;
    return True

n,i = 600851475143 ,2

while isPrime(n) == False:
    
    while isPrime(i) == False:
        i = i+1
         
    while n%i == 0 and n!=i:
        n = n/i
    
    i = i+1

print n

'''
##good solution 

roots = []; product = 1; x = 2; number = input("number?: "); y = number
while product != number:
    while (y % x == 0):
        roots.append(x)
        y /= x
        product *= roots[-1]
    x += 1
print roots
'''
        