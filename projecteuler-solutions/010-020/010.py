import math

def isPrime(n):
    for i in range(2,int(round(math.sqrt(n)))+1):
        if n%i == 0:
            return False
    return True

s = 0
for i in range(2, 2000000):
    if isPrime(i) == True:
        print i 
        s = s + i 

print s 