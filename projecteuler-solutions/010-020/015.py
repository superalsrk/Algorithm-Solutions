"""
The answer is C(2n)n, just because we need walk east  N steps, and walk souce N steps,
then choose N from 2N.

"""

s1 = reduce(lambda x,y:x*y,range(1,21))
s2 = reduce(lambda x,y:x*y,range(21,41))
print s2/s1
