import string

#notice that list*n is shadow copy
matrix = [[] for i in range(20)]

myfile = open("011.dat")
i = 0
while i < 20:
    list1 = myfile.readline()
    if list1 == "":
        break
    a = list1.split(" ")
    for x in range(len(a)):
        a[x] = string.atoi(a[x], 10)
        matrix[i]= a
    i = i + 1
    
maxVal = 0
for x in range(0, 20):
    for y in range(0, 17):
        tmp = matrix[x][y] * matrix[x][y + 1] * matrix[x][y + 2] * matrix[x][y + 3]
        if tmp > maxVal:
            maxVal = tmp

for x in range(0, 17):
    for y in range(0, 20):
        tmp = matrix[x][y] * matrix[x + 1][y] * matrix[x + 2][y] * matrix[x + 3][y]
        if tmp > maxVal:
            maxVal = tmp
            
for x in range(0, 17):
    for y in range(0, 17):
        tmp = matrix[x][y] * matrix[x + 1][y + 1] * matrix[x + 2][y + 2] * matrix[x + 3][y + 3]
        if tmp > maxVal:
            maxVal = tmp
            
for x in range(3, 20):
    for y in range(0, 17):
        tmp = matrix[x][y] * matrix[x - 1][y + 1] * matrix[x - 2][y + 2] * matrix[x - 3][y + 3]
        if tmp > maxVal:
            maxVal = tmp
print maxVal

# good solotions is to translate two-dimensional into one-dimensional array
# Only just to use split(' ') to get the array
# for example,if one number is ma[i],then it's adjacent number in down directon is
# ma[i+20],ma[i+40],ma[i+60], so do others directions