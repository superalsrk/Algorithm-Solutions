"""
projecteuler problem 13: Large Sum
"""

"""
Tips: the best way is to computer straightly

numbers =   " " "
37107287533902102798797998220837590246510135740250
...
53503534226472524250874054075591789781264330331690
" " ".strip().split('\n')
Problem13 = str(sum([int(i) for i in numbers]))[:10]

"""

#!/usr/bin/env python

class Largesum(object):
    def __init__(self):
        self.numdata = None
        with open('013.in') as f:
            self.numdata = f.readlines()
    
    def set_up(self):
        self.numdata = [list(num_str[:-1]) for num_str in self.numdata]
        self.numdata = [map(int,num_list)[::-1] for num_list in self.numdata]


    def start_compute(self):
        spill_num = 0
        result = []
        for l in range(50):
            tmp_sum = spill_num + sum([row[l] for row in self.numdata])
            spill_num = tmp_sum / 10
            result.append(tmp_sum % 10)
        
        return map(int,list(str(spill_num))) + result[::-1]


starter = Largesum()

if __name__ == '__main__':
    starter.set_up()
    res =  starter.start_compute()
    print ''.join(str(x) for x in res)[0:10]
        
            
