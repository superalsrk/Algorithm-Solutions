## use gcd and lcm
'''
def gcd(m,n):
    while m != 0:
        m,n = n%m,m
    return n

def lcm(m,n):
    return (m*(n/gcd(m,n)))

res = 1
for i in range(11,21):
    res = lcm(res,i)
print res
'''